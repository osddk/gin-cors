// Copyright (c) 2016 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

// https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
package gincors

import (
	"github.com/gin-gonic/gin"
	"net/url"
	"strings"
)

type Options struct {
	Origin []string // allowed origins
	Header []string // allowed headers
	Method []string // allowed methods
}

func Allow(opts Options) gin.HandlerFunc {
	return func(c *gin.Context) {
		h := c.Writer.Header()

		// Check origin against list of allowed origins.
		origin := c.Request.Header.Get("origin")
		if m := opts.allowedOrigin(origin); m != "" {
			h.Set("Access-Control-Allow-Origin", m)
		} else {
			c.JSON(400, map[string]string{"error": "origin_mismatch"})
			c.Abort()
			return
		}

		// If this is an OPTIONS request we generate preflight headers.
		if c.Request.Method == "OPTIONS" {
			h.Add("Access-Control-Allow-Headers", strings.Join(opts.Header, ", "))
			h.Add("Access-Control-Allow-Methods", strings.Join(opts.Method, ", "))
			c.Abort()
		}
	}
}

func (opts Options) allowedOrigin(origin string) string {
	// Parse origin and return empty string if parse fails.
	o, err := url.Parse(origin)
	if err != nil {
		return ""
	}

	// Iterate through allowed origins.
	m := ""
	for _, v := range opts.Origin {
		// Check for wildcard before parsing and return if true.
		if v == "*" {
			m = v
			break
		}

		// Parse allowed origin and match request origin.
		// TODO(taisph): parse urls *once* on init for improved performance
		u, _ := url.Parse(v)
		if (u.Scheme == "" || u.Scheme == o.Scheme) &&
			u.Host == o.Host && u.Path == o.Path {
			m = o.String()
			break
		}
	}
	return m
}
